import { Form, Input, InputNumber, Button, PageHeader } from 'antd';
import React, {useContext, useState} from "react"
import axios from 'axios';
import {Redirect} from "react-router-dom";
import Foot from '../Layout/Foot'
import {UserContext} from '../Auth/UserContext'

const layout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 20,
  },
};
const validateMessages = {
  required: '${label} harus diisi!',
  types: {
    number: '${label} harus angka!',
  },
  number: {
    range: '${label} harus di antara ${min} dan ${max}',
  },
};

const TambahMovie = () => {
	
  const [redirect, setRedirect] = useState(false)
  const [user] = useContext(UserContext)
	
  const onFinish = (values) => {
	  axios.post(`https://backendexample.sanbersy.com/api/data-movie`, {...values}, {headers: {"Authorization" : `Bearer ${user.token}`}})
	  .then(res => {
        // lakukan handle ketika sukses
		console.log(res.data);
		alert("Tambah Movie Berhasil!");
		setRedirect(true);
      }).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  };
	
  return (
  <>
  {redirect ?  <Redirect to='/TabelMovie' /> 
			:
	<>
	<PageHeader
	className="site-page-header"
	onBack={() => {setRedirect(true)}}
	title="Kembali ke Tabel Movie"
	/>
	<h1 style={{textAlign: 'center'}}>Form Tambah Movie</h1>
    <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} style={{border: "1px solid", "border-radius": "10px", width: "80%", padding: "5px", margin: "10px auto", marginBottom: "100px"}}>
      <Form.Item
        name='title'
        label="Judul Film"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
	  <Form.Item
        name='genre'
        label="Genre"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
	  <Form.Item
        name='year'
        label="Tahun Rilis"
        rules={[
          {
			required: true,
            type: 'number',
            min: 1900,
            max: 2020,
          },
        ]}
      >
        <InputNumber min={1900} max={2020}/>
      </Form.Item>
      <Form.Item
        name='rating'
        label="Rating"
        rules={[
          {
			required: true,
            type: 'number',
            min: 0,
            max: 10,
          },
        ]}
      >
        <InputNumber min={0} max={10}/>
      </Form.Item>
	  <Form.Item
        name="duration"
        label="Durasi"
        rules={[
          {
			type: 'number',
			required: true,
          },
        ]}
      >
        <InputNumber min={0}/>
      </Form.Item>
      <Form.Item name='description' label="Deskripsi">
        <Input.TextArea />
      </Form.Item>
	  <Form.Item name='review' label="Review">
        <Input.TextArea />
      </Form.Item>
	  <Form.Item name='image_url' label="URL Poster Film">
        <Input.TextArea />
      </Form.Item>
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 2 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
	<Foot />
	</>
	}
  </>
  );
};

export default TambahMovie