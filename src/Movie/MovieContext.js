import React, { useState, createContext } from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {
  const [dataMovie, setDataMovie] =  useState(null)

  return (
    <MovieContext.Provider value={[dataMovie, setDataMovie]}>
      {props.children}
    </MovieContext.Provider>
  );
};