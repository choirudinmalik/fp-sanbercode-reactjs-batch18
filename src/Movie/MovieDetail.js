import React, {useEffect, useState, useContext} from "react"
import {useParams} from "react-router-dom"
import axios from "axios"
import { Layout, Card, Col, Row, Rate} from 'antd';
import Nav from '../Layout/Nav';
import Foot from '../Layout/Foot'
import Sidebar from '../Layout/Sidebar'
import {UserContext} from '../Auth/UserContext'

const { Content } = Layout;
const { Meta } = Card;

const MovieDetail = ()=>{
  let {id} = useParams()
  const [data, setData] = useState(null)
  const [user] = useContext(UserContext);
  
  useEffect(() => {
    if (data === null){
      axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
      .then(res => {
        setData(res.data)
      })
    }
  }, [data, setData, id]);


  return (
  <>
  <Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			{user ? <Sidebar /> : null}
			<Layout className="site-layout">
				<Content style={{ margin: '0 16px', marginTop: 74, marginBottom: 100 }}>
					<Row>
					{ data !== null && (
						<>
							<Col span={12} style={{marginBottom: 20}}>
								<img src={data.image_url} alt="Poster Film" width='90%' height='450'/>
							</Col>
							<Col span={12} style={{marginBottom: 20}}>
								  <Card style={{height:'450'}}>
									<Meta
									  title={data.title}
									  description={
										  <>
										  <div style={{textAlign: 'center', color: 'blue'}}>  
											Rating: {data.rating}
											<br/>
											<Rate disabled value={data.rating} count='10'  />
										  </div>
										  <div style={{textAlign: 'center', color: 'green'}}>  
											Genre: {data.genre}
										  </div>
										  <div style={{textAlign: 'center', color: 'blue'}}>  
											Durasi: {parseInt(data.duration/60)} Jam {data.duration%60} Menit
										  </div>
										  <div style={{textAlign: 'center', color: 'green'}}>  
											Tahun Rilis: {data.year}
										  </div>
										  </>
									  }
									/>
									<div style={{'border-bottom': '1px solid', marginBottom: '20px', marginTop: '30px'}}>
										<h3>Description:</h3> 
										{data.description}
									</div>
									<div style={{'border-bottom': '1px solid'}}>
										<h3>Review:</h3> 
										{data.review}
									</div>
								  </Card>
							</Col>
						</>
						)}
					</Row>
				</Content>
				<Foot />
			</Layout>
		</Layout>
	  </Layout>	
	</>
  )
}

export default MovieDetail
