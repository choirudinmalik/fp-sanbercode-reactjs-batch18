import React, {useEffect, useContext} from "react"
import {Link} from "react-router-dom";
import { Layout, Card, Col, Row, Rate} from 'antd';
import axios from 'axios';
import Nav from '../Layout/Nav';
import Foot from '../Layout/Foot'
import Sidebar from '../Layout/Sidebar'
import {MovieContext} from "./MovieContext"
import {UserContext} from '../Auth/UserContext'

const { Meta } = Card;
const { Content } = Layout;


const Movie = () =>{
  const [dataMovie, setDataMovie] = useContext(MovieContext)
  const [user] = useContext(UserContext);
  
  
  useEffect( () => {
	if (dataMovie === null){
		axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
		.then(res => {
		  // lakukan pengolahan data
		  console.log(res.data);
		  setDataMovie(res.data);
		})
	}
  })
  
  return(
		<Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			{user ? <Sidebar /> : null}
			<Layout className="site-layout">
				<Content style={{ margin: '0 16px', marginTop: 74, marginBottom: 100 }}>
					<div className="site-card-wrapper">
						<Row>
						{ dataMovie !== null && (
						dataMovie.map((item, index)=> {
							  return (
								<Col span={8} style={{marginBottom: 20}}>
								  <Card style={{ width: 350}}
									cover={
									  <img
										alt="Poster Film"
										src={item.image_url}
										height='300'
									  />
									}
									actions={[
										<Link to={`/Movie/${item.id}`}>Click for Details</Link>,
									]}
								  >
									<Meta
									  title={item.title}
									  description={
										  <>
										  <div style={{textAlign: 'center', color: 'blue'}}>  
											<Rate disabled value={item.rating} count='10'  />
											<br/>
											Rating: {item.rating}
										  </div>
										  <div style={{textAlign: 'center', color: 'green'}}>  
											Genre: {item.genre}
										  </div>
										  </>
									  }
									/>
								  </Card>
								</Col>
							)})
						)}
						</Row>
					</div>
				</Content>
				<Foot />
			</Layout>
		</Layout>
	  </Layout>	
  )

}

export default Movie