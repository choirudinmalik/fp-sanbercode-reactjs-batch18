import React, {useEffect, useContext, useState} from "react"
import axios from 'axios';
import Nav from '../Layout/Nav';
import Foot from '../Layout/Foot'
import Sidebar from '../Layout/Sidebar'
import {Link} from "react-router-dom"
import {MovieContext} from "./MovieContext"
import {UserContext} from '../Auth/UserContext'
import { Table, Layout, Input, Popover, Button, Form, InputNumber, Select } from 'antd';
import { FilterOutlined, PlusCircleOutlined } from '@ant-design/icons';


const { Content } = Layout;
const { Search } = Input;
const { Option } = Select;

const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 20,
  },
};

const TabelMovie = () =>{
  const [dataMovie, setDataMovie] = useContext(MovieContext)
  const [visible, setVisible] = useState(false)
  const [user] = useContext(UserContext);
  
  const onSearch = value => {
	  axios.get(`https://www.backendexample.sanbersy.com/api/data-movie`)
    .then(res => {
      let filteredMovies = res.data.filter(item=> item.title.toLowerCase().includes(value.toLowerCase()))
      setDataMovie([...filteredMovies]);
	  })
  }
  
  const hide = () => {
    setVisible(false);
  };
  
  const onFilter = (values) => {
	  axios.get(`https://www.backendexample.sanbersy.com/api/data-movie`)
    .then(res => {
      let filteredMovies = res.data.filter(item=> item.genre.toLowerCase().includes(values.genre.toLowerCase()))
	  if (values.year !== ""){
		filteredMovies = filteredMovies.filter(item => item.year === values.year)
	  }
	  if (values.rating !== ""){
		filteredMovies = filteredMovies.filter(item => item.rating === values.rating)
	  }
	  filteredMovies = filteredMovies.filter((item) => {
		  switch (values.duration){
			  case "less":
				return item.duration < 90
			  case "between":
				return (item.duration >= 90) && (item.duration <= 120)
			  case "more":
				return item.duration > 120
			  default :
				return true
		  }
	  })
      setDataMovie([...filteredMovies]);
	  })
  }

  const  handleVisibleChange = visible => {
    setVisible(visible);
  };
  
  const handleHapus = (event) =>{
	  let index = parseInt(event.target.value)
	  axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${index}`, {headers: {"Authorization" : `Bearer ${user.token}`}})
      .then(res => {
        console.log(res.data);
		let dataBaru = dataMovie.filter(item=> item.id !== index);
		setDataMovie([...dataBaru]);
		alert("Hapus Data Berhasil");
	  }).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  }
  
  useEffect(() => {
    if (dataMovie === null){
      axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
      .then(res => {
        setDataMovie(res.data)
      })
    }
	
  }, [dataMovie, setDataMovie]);
  
const columns = [
  {
    title: 'Judul Film',
    dataIndex: 'title',
    sorter: (a, b) => (a.title === null || b.title === null)? false :a.title.localeCompare(b.title),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  return str.slice(0, 30) + '...'
		}
    },
  },
  {
    title: 'Tahun Rilis',
    dataIndex: 'year',
    sortDirections: ['descend', 'ascend', 'descend'],
    sorter: (a, b) => (a.year === null || b.year === null)? false :a.year - b.year,
  },
  {
    title: 'Rating',
    dataIndex: 'rating',
    sortDirections: ['descend', 'ascend', 'descend'],
    sorter: (a, b) => (a.rating === null || b.rating === null)? false :a.rating - b.rating,
  },
  {
    title: 'Durasi',
    dataIndex: 'duration',
    sortDirections: ['descend', 'ascend', 'descend'],
    sorter: (a, b) => (a.duration === null || b.duration === null)? false :a.duration - b.duration,
  },
  {
    title: 'Genre',
    dataIndex: 'genre',
    sorter: (a, b) => (a.genre === null || b.genre === null)? false : a.genre.localeCompare(b.genre),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  let arr = str.split(", ")
		  let i = 0
		  let newStr = ""
		  while(i < arr.length && newStr.length <= 30){
			  let temp = newStr
			  newStr += (arr[i] + ", ")  
			  if (newStr.length > 30){
				  newStr = temp
			  }
			  i++
		  }
		  return newStr + '...'
		}
    },
  },
  {
    title: 'Deskripsi',
    dataIndex: 'description',
    sorter: (a, b) => (a.description === null || b.description === null)? false : a.description.localeCompare(b.description),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  return str.slice(0, 30) + '...'
		}
    },
  },
  {
    title: 'Review',
    dataIndex: 'review',
    sorter: (a, b) => (a.review === null || b.review === null)? false : a.review.localeCompare(b.review),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  return str.slice(0, 30) + '...'
		}
    },
  },
  {
    title: 'Action',
    dataIndex: 'id',
    render: (id) => (
      <>
		<button><Link to={`/Movie/${id}`}>Details</Link></button>
        <button><Link to={`/Movie/Edit/${id}`}>Edit</Link></button>
		<button value={id} onClick={handleHapus}>Hapus</button>
      </>
    ),
  },
];

  return(
    <Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			<Sidebar />
			<Layout className="site-layout">
				<Content style={{ margin: '0 16px', marginTop: 74, marginBottom: 100 }}>
					<Table 
					columns={columns} 
					dataSource={dataMovie} 
					bordered='true'
					pagination={{ pageSize: 3 }}					
					title={() => 
						<>
							<h2 style={{color:'Blue', display: 'inline-block'}}>Tabel Movie</h2>
							<Search style={{float:'right', width: 300, margin: '0 10px'}} placeholder="Cari Berdasarkan Judul" onSearch={onSearch} enterButton />
							<button style={{float:'right', display: 'inline-block'}}><PlusCircleOutlined /> <Link to="/TambahMovie">Tambah Movie</Link></button>
							<Popover
								content={
									<>
									<Form {...layout} name="nest-messages" onFinish={onFilter}
									initialValues={{
										genre: "",
										rating: "",
										year: "",
										duration: "",
									  }}
									>
									  <Form.Item
										name="genre"
										label="Genre"
									  >
									  <Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataMovie !== null && (
											[...new Set([...(dataMovie.map((item)=>{
												return (
													item.genre.split(", ")
												)
											})
											)].flat(1)
											)].map((item)=>{
												return(
													<Option value={item}>{item}</Option>
												)
											})
										)}											
									  </Select>
									  </Form.Item>
									  <Form.Item
										name='year'
										label="Tahun Rilis"
									  >
										<Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataMovie !== null && (
											[...new Set(dataMovie.map((x)=> x.year))].map((item)=>{
												return(
											<Option value={item}>{item}</Option>
												)
											})
										)}											
										</Select>
									  </Form.Item>
									  <Form.Item
										name='rating'
										label="Rating"
									  >
										<Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataMovie !== null && (
											[...new Set(dataMovie.map((x)=> x.rating))].map((item)=>{
												return(
											<Option value={item}>{item}</Option>
												)
											})
										)}											
										</Select>
									  </Form.Item>
									  <Form.Item
										name="duration"
										label="Durasi"
									  >
										<Select style={{ width: 200 }}>
											<Option value="">All</Option>
											<Option value="less"> &lt;90 </Option>
											<Option value="between"> 90 &lt;= x &gt;= 120 </Option>
											<Option value="more"> &gt;120 </Option>
										</Select>
									  </Form.Item>
									  <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 2 }}>
										<Button type="primary" htmlType="submit">
										  Submit
										</Button>
									  </Form.Item>
									</Form>
									<Button style={{marginLeft:"300px"}} onClick={hide} type="primary">Close</Button>
									</>
								}
								title="Filter Tabel"
								trigger="click"
								visible={visible}
								onVisibleChange={handleVisibleChange}
								overlayStyle={{width:'400px'}}
							  >
								<Button style={{float:'right', display: 'inline-block', marginRight: 10}} icon={<FilterOutlined />} type="primary">Filter</Button>
							</Popover>
						</>
					} 
					/>
				</Content>
				<Foot />
			</Layout>
		</Layout>
	  </Layout>
  )
}

export default TabelMovie
