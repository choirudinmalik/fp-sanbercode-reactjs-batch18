import React, { useState, createContext } from "react";

export const GameContext = createContext();

export const GameProvider = props => {
  const [dataGame, setDataGame] =  useState(null)

  return (
    <GameContext.Provider value={[dataGame, setDataGame]}>
      {props.children}
    </GameContext.Provider>
  );
};