import React, {useEffect, useContext, useState} from "react"
import axios from 'axios';
import Nav from '../Layout/Nav';
import Foot from '../Layout/Foot'
import Sidebar from '../Layout/Sidebar'
import {Link} from "react-router-dom"
import {GameContext} from "./GameContext"
import {UserContext} from '../Auth/UserContext'
import { Table, Layout, Input, Popover, Button, Form, InputNumber, Select, Row, Col, Checkbox } from 'antd';
import { FilterOutlined, PlusCircleOutlined } from '@ant-design/icons';

const { Content } = Layout;
const { Search } = Input;
const { Option } = Select;

const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 20,
  },
};

const TabelGame = () =>{
  const [dataGame, setDataGame] = useContext(GameContext)
  const [visible, setVisible] = useState(false)
  const [user] = useContext(UserContext);
  
  const onSearch = value => {
	  axios.get(`https://www.backendexample.sanbersy.com/api/data-game`)
    .then(res => {
      let filteredGame = res.data.filter(item=> item.name.toLowerCase().includes(value.toLowerCase()))
      setDataGame([...filteredGame]);
	  })
  }
  
  const hide = () => {
    setVisible(false);
  };

  const  handleVisibleChange = visible => {
    setVisible(visible);
  };
  
  const onFilter = (values) => {
	  axios.get(`https://www.backendexample.sanbersy.com/api/data-game`)
    .then(res => {
      let filteredGame = res.data.filter(item=> item.genre.toLowerCase().includes(values.genre.toLowerCase()))
	  
	  filteredGame = filteredGame.filter(item=> item.platform.toLowerCase().includes(values.platform.toLowerCase()))
	  if (values.release !== ""){
		filteredGame = filteredGame.filter(item => item.release === values.release)
	  }
	  if (!values.player.includes("All")){
		  let single = false;
		  let multi = false;
		  if (values.player !== undefined){
			single = values.player.includes("SinglePlayer");
			multi = values.player.includes("Multiplayer");
		  }
		  filteredGame = filteredGame.filter(item=> item.singlePlayer == single)
		  filteredGame = filteredGame.filter(item=> item.multiplayer == multi)
	  }
	  setDataGame([...filteredGame]);
	  })
  }
  
  const handleHapus = (event) =>{
	  let index = parseInt(event.target.value)
	  axios.delete(`https://backendexample.sanbersy.com/api/data-game/${index}`, {headers: {"Authorization" : `Bearer ${user.token}`}})
      .then(res => {
        console.log(res.data);
		let dataBaru = dataGame.filter(item=> item.id !== index);
		setDataGame([...dataBaru]);
		alert("Hapus Data Berhasil");
	  }).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  }
  
  useEffect(() => {
    if (dataGame === null){
      axios.get(`https://backendexample.sanbersy.com/api/data-game`)
      .then(res => {
        setDataGame(res.data)
      })
    }
	
  }, [dataGame, setDataGame]);
  
  const columns = [
  {
    title: 'Nama Game',
    dataIndex: 'name',
    sorter: (a, b) => (a.name === null || b.name === null)? false :a.name.localeCompare(b.name),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  return str.slice(0, 30) + '...'
		}
    },
  },
  {
    title: 'Tahun Rilis',
    dataIndex: 'release',
    sortDirections: ['descend', 'ascend', 'descend'],
    sorter: (a, b) => (a.year === null || b.year === null)? false : parseInt(a.year) - parseInt(b.year),
  },
  {
    title: 'Genre',
    dataIndex: 'genre',
    sorter: (a, b) => (a.genre === null || b.genre === null)? false : a.genre.localeCompare(b.genre),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  let arr = str.split(", ")
		  let i = 0
		  let newStr = ""
		  while(i < arr.length && newStr.length <= 30){
			  let temp = newStr
			  newStr += (arr[i] + ", ")  
			  if (newStr.length > 30){
				  newStr = temp
			  }
			  i++
		  }
		  return newStr + '...'
		}
    },
  },
  {
    title: 'Platform',
    dataIndex: 'platform',
    sorter: (a, b) => (a.platform === null || b.platform === null)? false : a.platform.localeCompare(b.platform),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: str => {
		if (str === null){
		  return ""
		}else{
		  if (str.length <= 30) {
			return str
		  }
		  let arr = str.split(", ")
		  let i = 0
		  let newStr = ""
		  while(i < arr.length && newStr.length <= 30){
			  let temp = newStr
			  newStr += (arr[i] + ", ")  
			  if (newStr.length > 30){
				  newStr = temp
			  }
			  i++
		  }
		  return newStr + '...'
		}
    },
  },
  {
    title: 'Single Player',
    dataIndex: 'singlePlayer',
    sorter: (a, b) => (a.singlePlayer === null || b.singlePlayer === null)? false : parseInt(a.singlePlayer) - parseInt(b.singlePlayer),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: bool => {
		if (bool){
		  return "yes"
		}else{
		  return "no"
		}
    },
  },
  {
    title: 'Multiplayer',
    dataIndex: 'multiplayer',
    sorter: (a, b) => (a.multiplayer === null || b.multiplayer === null)? false : parseInt(a.multiplayer) - parseInt(b.multiplayer),
    sortDirections: ['descend', 'ascend', 'descend'],
	render: bool => {
		if (bool){
		  return "yes"
		}else{
		  return "no"
		}
    },
  },
  {
    title: 'Action',
    dataIndex: 'id',
    render: (id) => (
      <>
		<button><Link to={`/Game/${id}`}>Details</Link></button>
        <button><Link to={`/Game/Edit/${id}`}>Edit</Link></button>
		<button value={id} onClick={handleHapus}>Hapus</button>
      </>
    ),
  },
];

  return(
    <Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			<Sidebar />
			<Layout className="site-layout">
				<Content style={{ margin: '0 16px', marginTop: 74, marginBottom: 100 }}>
					<Table 
					columns={columns} 
					dataSource={dataGame} 
					bordered='true'
					pagination={{ pageSize: 3 }}					
					title={() => 
						<>
							<h2 style={{color:'Blue', display: 'inline-block'}}>Tabel Game</h2>
							<Search style={{float:'right', width: 300, margin: '0 10px'}} placeholder="Cari Berdasarkan Nama" onSearch={onSearch} enterButton />
							<button style={{float:'right', display: 'inline-block'}}><PlusCircleOutlined /> <Link to="/TambahGame">Tambah Game</Link></button>
							<Popover
								content={
									<>
									<Form {...layout} name="nest-messages" onFinish={onFilter}
									initialValues={{
										genre: "",
										platform: "",
										release: "",
										player: ["All"],
									  }}
									>
									  <Form.Item
										name="genre"
										label="Genre"
									  >
									  <Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataGame !== null && (
											[...new Set([...(dataGame.map((item)=>{
												return (
													item.genre.split(", ")
												)
											})
											)].flat(1)
											)].map((item)=>{
												return(
													<Option value={item}>{item}</Option>
												)
											})
										)}											
									  </Select>
									  </Form.Item>
									  <Form.Item
										name="platform"
										label="Platform"
									  >
									  <Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataGame !== null && (
											[...new Set([...(dataGame.map((item)=>{
												return (
													item.platform.split(", ")
												)
											})
											)].flat(1)
											)].map((item)=>{
												return(
													<Option value={item}>{item}</Option>
												)
											})
										)}											
									  </Select>
									  </Form.Item>
									  <Form.Item
										name='release'
										label="Tahun Rilis"
									  >
										<Select style={{ width: 200 }}>
											<Option value="">All</Option>
										{ dataGame !== null && (
											[...new Set(dataGame.map((x)=> x.release))].map((item)=>{
												return(
											<Option value={item}>{item}</Option>
												)
											})
										)}											
										</Select>
									  </Form.Item>
									  <Form.Item name="player" label="Player Type">
										<Checkbox.Group>
										  <Row>
											<Col span={32}>
											  <Checkbox
												value="SinglePlayer"
												style={{ 
												  lineHeight: '32px',
												}}
											  >
												Single Player
											  </Checkbox>
											</Col>
											<Col span={32}>
											  <Checkbox
												value="Multiplayer"
												style={{
												  lineHeight: '32px',
												}}
											  >
												Multiplayer
											  </Checkbox>
											</Col>
											<Col span={32}>
											  <Checkbox
												value="All"
												style={{
												  lineHeight: '32px',
												}}
											  >
												Disable Player Filter
											  </Checkbox>
											</Col>
										  </Row>
										</Checkbox.Group>
									  </Form.Item>
									  <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 2 }}>
										<Button type="primary" htmlType="submit">
										  Submit
										</Button>
									  </Form.Item>
									</Form>
									<Button style={{marginLeft:"300px"}} onClick={hide} type="primary">Close</Button>
									</>
								}
								title="Filter Tabel"
								trigger="click"
								visible={visible}
								onVisibleChange={handleVisibleChange}
								overlayStyle={{width:'400px'}}
							  >
								<Button style={{float:'right', display: 'inline-block', marginRight: 10}} icon={<FilterOutlined />} type="primary">Filter</Button>
							</Popover>
						</>
					} 
					/>
				</Content>
				<Foot />
			</Layout>
		</Layout>
	  </Layout>
  )
}

export default TabelGame
