import React, {useEffect, useState, useContext} from "react"
import {useParams} from "react-router-dom"
import axios from "axios"
import { Layout, Card, Col, Row} from 'antd';
import Nav from '../Layout/Nav';
import Foot from '../Layout/Foot'
import Sidebar from '../Layout/Sidebar'
import {UserContext} from '../Auth/UserContext'
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';

const { Content } = Layout;
const { Meta } = Card;

const GameDetail = ()=>{
  let {id} = useParams()
  const [data, setData] = useState(null)
  const [user] = useContext(UserContext);
  
  useEffect(() => {
    if (data === null){
      axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
      .then(res => {
        setData(res.data)
      })
    }
  }, [data, setData, id]);


  return (
  <>
  <Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			{user ? <Sidebar /> : null}
			<Layout className="site-layout">
				<Content style={{ margin: '0 16px', marginTop: 74, marginBottom: 100 }}>
					<Row>
					{ data !== null && (
						<>
							<Col span={12} style={{marginBottom: 20}}>
								<img src={data.image_url} alt="Poster Game" width='90%' height='450'/>
							</Col>
							<Col span={12} style={{marginBottom: 20}}>
								  <Card style={{height:'450'}}>
									<Meta
									  title={data.name}
									  description={
										  <>
										  <div style={{textAlign: 'center', color: 'green'}}>  
											Genre: {data.genre}
										  </div>
										  <div style={{textAlign: 'center', color: 'blue'}}>  
											Platfotm: {data.platform}
										  </div>
										  <div style={{textAlign: 'center', color: 'green'}}>  
											Tahun Rilis: {data.release}
										  </div>
										  </>
									  }
									/>
									<div style={{'border-top': '1px solid', marginBottom: '20px', marginTop: '30px'}}>
										<h3>Single Player {data.singlePlayer? <CheckCircleOutlined /> : <CloseCircleOutlined />}</h3> 
										<h3>Multiplayer {data.multiplayer? <CheckCircleOutlined /> : <CloseCircleOutlined />}</h3>
									</div>
								  </Card>
							</Col>
						</>
						)}
					</Row>
				</Content>
				<Foot />
			</Layout>
		</Layout>
	  </Layout>	
	</>
  )
}

export default GameDetail
