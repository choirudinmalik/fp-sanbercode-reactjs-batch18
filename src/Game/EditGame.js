import { Form, Input, Button, PageHeader, Checkbox, Row, Col, } from 'antd';
import React, {useContext, useState, useEffect} from "react"
import axios from 'axios';
import {Redirect, useParams} from "react-router-dom";
import Foot from '../Layout/Foot'
import {UserContext} from '../Auth/UserContext'

const layout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 20,
  },
};
const validateMessages = {
  required: '${label} harus diisi!',
  types: {
    number: '${label} harus angka!',
  },
  number: {
    range: '${label} harus di antara ${min} dan ${max}',
  },
};

const EditGame = () => {
	
  let {id} = useParams()
  const [data, setData] = useState(null)
  const [redirect, setRedirect] = useState(false)
  const [user] = useContext(UserContext)
	
  useEffect(() => {
    if (data === null){
      axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
      .then(res => {
        setData(res.data)
      })
    }
  }, [data, setData, id]);
  
  const onFinish = (values) => {
	  let single = values.player.includes("SinglePlayer");
	  let multi = values.player.includes("Multiplayer");
	  let tambah = {
			genre: values.genre,
			image_url: values.image_url,
			singlePlayer: single,
			multiplayer: multi,
			name: values.name,
			platform: values.platform,
			release: values.release,
	  }
	  axios.put(`https://backendexample.sanbersy.com/api/data-game/${id}`, tambah, {headers: {"Authorization" : `Bearer ${user.token}`}})
	  .then(res => {
        // lakukan handle ketika sukses
		console.log(res.data);
		alert("Edit Game Berhasil!");
		setRedirect(true);
      }).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  };
	
  return (
  <>
  {redirect ?  <Redirect to='/TabelGame' /> 
			:
	<>
	<PageHeader
	className="site-page-header"
	onBack={() => {setRedirect(true)}}
	title="Kembali ke Tabel Game"
	/>
	<h1 style={{textAlign: 'center'}}>Form Tambah Game</h1>
	{ data !== null && (
	<>
    <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} 
	style={{border: "1px solid", "border-radius": "10px", width: "80%", padding: "5px", margin: "10px auto", marginBottom: "100px"}}
	initialValues={{
        name: data.name,
        genre: data.genre,
        platform: data.platform,
		release: data.release,
		player: (data.singlePlayer && data.multiplayer) ? ['SinglePlayer', 'Multiplayer'] : (data.singlePlayer ? ['SinglePlayer'] : (data.multiplayer ? ['Multiplayer'] : null)),
		image_url: data.image_url,
      }}
	>
      <Form.Item
        name='name'
        label="Nama Game"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
	  <Form.Item
        name='genre'
        label="Genre"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
	  <Form.Item
        name='release'
        label="Tahun Rilis"
        rules={[
          {
			required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='platform'
        label="Platform"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
	  <Form.Item name="player" label="Player Type">
        <Checkbox.Group>
          <Row>
            <Col span={32}>
              <Checkbox
                value="SinglePlayer"
                style={{ 
                  lineHeight: '32px',
                }}
              >
                Single Player
              </Checkbox>
            </Col>
            <Col span={32}>
              <Checkbox
                value="Multiplayer"
                style={{
                  lineHeight: '32px',
                }}
              >
                Multiplayer
              </Checkbox>
            </Col>
          </Row>
        </Checkbox.Group>
      </Form.Item>
	  <Form.Item name='image_url' label="URL Poster Film">
        <Input.TextArea />
      </Form.Item>
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 2 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
	</>
	)}
	<Foot />
	</>
	}
  </>
  );
};

export default EditGame