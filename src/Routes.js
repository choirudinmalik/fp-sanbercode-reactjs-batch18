import React, {useContext} from "react";
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Main from './Layout/Main'
import Login from './Auth/Login'
import Register from './Auth/Register'
import ChangePass from './Auth/ChangePass'
import Movie from './Movie/Movie'
import Game from './Game/Game'
import MovieDetail from './Movie/MovieDetail'
import GameDetail from './Game/GameDetail'
import TabelMovie from './Movie/TabelMovie'
import TabelGame from './Game/TabelGame'
import TambahMovie from './Movie/TambahMovie'
import TambahGame from './Game/TambahGame'
import EditMovie from './Movie/EditMovie'
import EditGame from './Game/EditGame'
import {UserContext} from './Auth/UserContext' 
import {MovieProvider} from './Movie/MovieContext'
import {GameProvider} from './Game/GameContext'
import FreePass from './Layout/FreePass'

const Routes = () => {
  const [user] = useContext(UserContext);

  const PrivateRoute = ({user, ...props }) => {
    if (user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };
  
  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;


  return (
    <Switch>
	  <Route exact path="/">
        <Main />
      </Route>
	  <Route exact path="/redirect">
        <FreePass />
      </Route>
	  <LoginRoute exact path="/Login" user={user}>
        <Login />
      </LoginRoute>
	  <LoginRoute exact path="/Register" user={user}>
        <Register />
      </LoginRoute> 
	  <Route exact path="/Movie">
		<MovieProvider>	
			<Movie />
		</MovieProvider>
	  </Route>
	  <Route exact path="/Movie/:id">
		<MovieDetail />
	  </Route>
	  <PrivateRoute exact path="/TabelMovie" user={user}>
		<MovieProvider>	
			<TabelMovie />
		</MovieProvider>
	  </PrivateRoute>
	  <PrivateRoute exact path="/TambahMovie" user={user}>
		<MovieProvider>	
			<TambahMovie />
		</MovieProvider>
	  </PrivateRoute>
	  <PrivateRoute exact path="/Movie/Edit/:id" user={user}>
		<EditMovie />
	  </PrivateRoute>
	  <Route exact path="/Game">
		<GameProvider>
			<Game />
		</GameProvider>
	  </Route>
	  <Route exact path="/Game/:id">
		<GameDetail />
	  </Route>
	  <PrivateRoute exact path="/TabelGame" user={user}>
		<GameProvider>	
			<TabelGame />
		</GameProvider>
	  </PrivateRoute>
	  <PrivateRoute exact path="/TambahGame" user={user}>
		<GameProvider>	
			<TambahGame />
		</GameProvider>
	  </PrivateRoute>
	  <PrivateRoute exact path="/Game/Edit/:id" user={user}>
		<EditGame />
	  </PrivateRoute>
	  <PrivateRoute exact path="/ChangePass" user={user}>
		<ChangePass />
	  </PrivateRoute>
    </Switch>
  );
};

export default Routes;