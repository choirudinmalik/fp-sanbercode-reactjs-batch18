import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import { BrowserRouter as Router } from "react-router-dom";
import {UserProvider} from "./Auth/UserContext"
import Routes from './Routes'

function App() {
  return (
	<>
		<UserProvider>
			<Router>
				<Routes />
			</Router>
		</UserProvider>
	</>
  );
}

export default App;
