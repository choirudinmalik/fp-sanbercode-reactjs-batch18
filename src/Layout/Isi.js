import { Layout } from 'antd';
import React, {useContext} from 'react';
import {UserContext} from '../Auth/UserContext'

const { Content } = Layout;

const Isi = () => {
	const [user] = useContext(UserContext);
    return (
		<Content style={{ margin: '0 16px', marginTop: 64 }}>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360, textAlign: 'center', fontSize: '70px' }}>
			{user ? 
			<>
			  <h1>Selamat Datang Kembali</h1>
			  <h1>{user.name}</h1>
			</>
			:
			<>
			  <h1>Selamat Datang di</h1>
			  <h1>Website Rekomendasi</h1>
			  <h1>Film dan Game Terbaik</h1>
            </>
			}
			</div>
          </Content>
	)
}

export default Isi;