import React from "react";
import { Redirect} from "react-router-dom";

const FreePass = () => {
	return(<Redirect to='/' />)
}

export default FreePass;