import { Layout} from 'antd';
import React, {useContext} from "react";
import Nav from './Nav';
import Isi from './Isi'
import Foot from './Foot'
import Sidebar from './Sidebar'
import {UserContext} from '../Auth/UserContext'

const Main = () => {
	
	const [user] = useContext(UserContext);
	
    return (
      <Layout className="layout">
		<Nav />
		<Layout style={{ minHeight: '100vh' }}>
			{user ? <Sidebar /> : null}
			
			<Layout className="site-layout">
				<Isi />
				<Foot />
			</Layout>
		</Layout>
	  </Layout>	
    )
  }

export default Main;