import { Layout, Menu } from 'antd';
import React from 'react';
import {
  DeploymentUnitOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import {Link} from "react-router-dom";

const { SubMenu } = Menu;
const { Sider } = Layout;

class Sidebar extends React.Component {
	
	constructor(props){
    super(props)
    this.state ={
     collapsed: true,
    }
	
	this.onCollapse = this.onCollapse.bind(this);
  }
	
  onCollapse (collapsed){
    this.setState({ collapsed });
  };

  render() {
	  const { collapsed } = this.state;
	  return (
		<Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse} style={{marginTop:64}}>
			<Menu theme="dark" defaultSelectedKeys={['0']} mode="inline">
            <SubMenu key="sub1" icon={<DeploymentUnitOutlined />} title="Game">
                <Menu.Item key="1">
					<Link to="/TabelGame">Tabel Game</Link>
				</Menu.Item>
				<Menu.Item key="2">
					<Link to="/TambahGame">Tambah Game</Link>
				</Menu.Item>
              </SubMenu>
            <SubMenu key="sub2" icon={<VideoCameraOutlined />} title="Movie">
              <Menu.Item key="3">
					<Link to="/TabelMovie">Tabel Movie</Link>
				</Menu.Item>
				<Menu.Item key="4">
					<Link to="/TambahMovie">Tambah Movie</Link>
				</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
    )
  }
}

export default Sidebar;