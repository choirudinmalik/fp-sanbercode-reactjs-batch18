import { Layout} from 'antd';
import React from 'react';

const {Footer } = Layout;

class Foot extends React.Component {
  render() {
    return (
		<Footer style={{ textAlign: 'center', position: 'fixed', bottom: 0, width: '100%', backgroundColor: '#081A38', color: 'white'  }}>
			Created By: Muhammad Choirudin Malik
			<br/>
			Contact Me: choirudinmalik@gmail.com/081299294807
		</Footer>	
    )
  }
}

export default Foot;