import { Layout, Menu } from 'antd';
import React, {useContext, useState} from "react";
import {Link, Redirect} from "react-router-dom";
import {UserContext} from '../Auth/UserContext' 

const { Header } = Layout;

const Nav = () => {
  const [user, setUser] = useContext(UserContext);
  const [redirect, setRedirect] = useState(false)
  
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
	setRedirect(true);
  }

    return (
	<>
	{redirect ?  <Redirect to='/redirect' /> 
			:
		<Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
		  <Menu theme="dark" mode="horizontal" DefaultSelectedKeys={['1']}>
			<Menu.Item key="1">
				<Link to="/">Home</Link>
			</Menu.Item>
			<Menu.Item key="2">
				<Link to="/Movie">Movie</Link>
			</Menu.Item>
			<Menu.Item key="3">
				<Link to="/Game">Game</Link>
			</Menu.Item>
			{user && 
            (
              <>
                <Menu.Item><Link to="/ChangePass">Change Password</Link></Menu.Item>
                <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
              </>
            )
          }
          {user === null && 
            (
              <>
                <Menu.Item><Link to="/Login">Login</Link></Menu.Item>
				<Menu.Item><Link to="/Register">Register</Link></Menu.Item>
              </>
            )
          }
		  </Menu>
		</Header>
	}
	</>
    )
}

export default Nav;