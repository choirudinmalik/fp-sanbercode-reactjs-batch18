import React, { useContext, useState } from "react"
import {Card, PageHeader } from 'antd';
import {Link, Redirect} from "react-router-dom";
import Foot from '../Layout/Foot'

import {UserContext} from "./UserContext"
import axios from "axios"

const Login = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({email: "" , password: ""})
  const [redirect, setRedirect] = useState(false)
  
  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post("https://backendexample.sanbersy.com/api/user-login", {
      email: input.email, 
      password: input.password
    }).then(
      (res)=>{
        var user = res.data.user
        var token = res.data.token
        var currentUser = {name: user.name, email: user.email, token }
        setUser(currentUser)
        localStorage.setItem("user", JSON.stringify(currentUser))
		alert("Login Berhasil")
      }
    ).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
	{redirect ?  <Redirect to='/' /> 
			:
	<>
									<PageHeader
										className="site-page-header"
										onBack={() => {setRedirect(true)}}
										title="Kembali ke Home"
									/>
	<Card title="Login Form" style={{ margin: "0 auto", width: "25%", padding: "50px", backgroundColor: "LightBlue" }}>
		<form onSubmit={handleSubmit}>
          <label>Email: </label>
          <input type="email" name="email" onChange={handleChange} value={input.email}/>
          <br/><br/>
          <label>Password: </label>
          <input type="password" name="password" onChange={handleChange} value={input.password}/>
          <br/><br/>
          <button style={{marginRight: '10px'}}>Login</button>
		  Or <Link to="/Register">Register Now!</Link>
        </form>
    </Card>
	<Foot />
	</>
	}
    </>
  )
}

export default Login
