import React, { useContext, useState } from "react"
import {UserContext} from "./UserContext"
import axios from "axios"
import {Card, PageHeader } from 'antd';
import { Redirect } from "react-router-dom";
import Foot from '../Layout/Foot'

const Register = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({name: "", email: "" , password: ""})
  const [redirect, setRedirect] = useState(false)
 
  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post("https://backendexample.sanbersy.com/api/register", {
      name: input.name, 
      email: input.email, 
      password: input.password
    }).then(
      (res)=>{
        var user = res.data.user
        var token = res.data.token
        var currentUser = {name: user.name, email: user.email, token }
        setUser(currentUser)
        localStorage.setItem("user", JSON.stringify(currentUser))
		alert("Register Berhasil")
		
      }
    ).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "name":{
        setInput({...input, name: value})
        break;
      }
      case "email":{
        setInput({...input, email: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
	{redirect ?  <Redirect to='/' /> 
			:
	<>
									<PageHeader
										className="site-page-header"
										onBack={() => {setRedirect(true)}}
										title="Kembali ke Home"
									/>
		<Card title="Register Form" style={{ margin: "0 auto", width: "25%", padding: "50px", backgroundColor: "LightBlue" }}>
        <form onSubmit={handleSubmit}>
          <label>name: </label>
          <input type="text" name="name" onChange={handleChange} value={input.name}/>
          <br/><br/>
          <label>email: </label>
          <input type="email" name="email" onChange={handleChange} value={input.email}/>
          <br/><br/>
          <label>Password: </label>
          <input type="password" name="password" onChange={handleChange} value={input.password}/>
          <br/><br/>
          <button>Register</button>
        </form>
      </Card>
	  <Foot />
	</>
	}
    </>
  )
}

export default Register