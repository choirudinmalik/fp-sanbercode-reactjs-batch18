import React, { useContext, useState } from "react"
import { Redirect } from "react-router-dom";
import {UserContext} from "./UserContext"
import axios from "axios"
import {Card, PageHeader } from 'antd';
import Foot from '../Layout/Foot'

const ChangePass = () =>{
  const [user] = useContext(UserContext)
  const [input, setInput] = useState({current_password: "" , new_password: "", new_confirm_password: ""})
  const [redirect, setRedirect] = useState(false)
  
  const handleSubmit = (event) =>{
    event.preventDefault()
    axios.post("https://backendexample.sanbersy.com/api/change-password", {
      current_password: input.current_password, 
      new_password: input.new_password,
	  new_confirm_password: input.new_confirm_password
    }, {headers: {"Authorization" : `Bearer ${user.token}`}}).then(
      (res)=>{
        console.log(res.data)
		alert("Ubah Password Berhasil")
		setRedirect(true);
      }
    ).catch((err)=>{
	  alert(JSON.stringify(err.response.data))
    })
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "current_password":{
        setInput({...input, current_password: value})
        break;
      }
      case "new_confirm_password":{
        setInput({...input, new_confirm_password: value})
        break;
      }
	  case "new_password":{
        setInput({...input, new_password: value})
        break;
      }
      default:{break;}
    }
  }

  return(
    <>
	{redirect ?  <Redirect to='/' /> 
			:
	<>
									<PageHeader
										className="site-page-header"
										onBack={() => {setRedirect(true)}}
										title="Kembali ke Home"
									/>
      <Card title="Form Ubah Password" style={{ margin: "0 auto", width: "25%", padding: "50px", backgroundColor: "LightBlue" }}>
        <form onSubmit={handleSubmit}>
          <label>Current Password: </label>
          <input type="password" name="current_password" onChange={handleChange} value={input.current_password}/>
          <br/><br/>
		  <label>New Password: </label>
          <input type="password" name="new_password" onChange={handleChange} value={input.new_password}/>
          <br/><br/>
		  <label>Confirm New Password: </label>
          <input type="password" name="new_confirm_password" onChange={handleChange} value={input.new_confirm_password}/>
          <br/><br/>
          <button>Change Password</button>
        </form>
      </Card>
	  <Foot />
	</>
	}
    </>
  )
}

export default ChangePass
